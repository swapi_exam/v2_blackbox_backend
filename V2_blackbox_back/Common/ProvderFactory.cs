﻿using V2_blackbox_back.Business;
using V2_blackbox_back.Enum;
using V2_blackbox_back.Interface;

namespace V2_blackbox_back.Common
{
    public class ProvderFactory
    {
        public static IProvider GetProvderFactory(string provider, IProviderService _ProviderService, IConfiguration configuration)
        {
            if (provider == ProviderEnum.Tokbox.ToString())
            {
                return new TokboxProvider(_ProviderService, configuration);
            }
            else if (provider == ProviderEnum.Twilio.ToString())
            {
                return new TwilioProvider();
            }
            return null;
        }
    }
}

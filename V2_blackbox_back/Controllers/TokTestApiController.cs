using Microsoft.AspNetCore.Mvc;
using V2_blackbox_back.Model;
using OpenTokSDK;
using System.Net;
using Microsoft.AspNetCore.Cors;
using V2_blackbox_back.Common;
using V2_blackbox_back.Interface;
using V2_blackbox_back.Enum;
using Twilio;
using System.Text;
using Newtonsoft.Json;
using Twilio.Rest.Video.V1.Room;
using Twilio.Rest.Video.V1;
using static Twilio.Rest.Video.V1.CompositionHookResource;

namespace V2_blackbox_back.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors]
    public class TokTestApiController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<TokTestApiController> _logger;
        private IProviderService _ProviderService;
        private readonly IConfiguration _configuration;

        public Session session { get; protected set; }
        public OpenTok OpenTok { get; protected set; }

        public int ApiKey = 47095304; // YOUR API KEY
        public string ApiSecret = "3ca713b0d132be02013218a35e152a733cea4c4c";

        public dynamic archive = null;

        public TokTestApiController(ILogger<TokTestApiController> logger, IProviderService providerService, IConfiguration configuration)
        {
            _logger = logger;
            _ProviderService = providerService;
            _configuration = configuration;
        }

        [HttpPost]
        //[HttpGet]
        public ActionResult Get([FromBody] ProviderRequestModel oProviderRequestModel)
        {
            IProvider objProvider = ProvderFactory.GetProvderFactory(oProviderRequestModel.ProviderName, _ProviderService, _configuration);

            ProviderResponseModel oProviderResponseModel = objProvider.GetProvider(oProviderRequestModel);
            return Ok(oProviderResponseModel);
        }

        [HttpPost("RecordStatus")]
        public ActionResult Post(ProviderRecordRequestModel oProviderRecordRequestModel)
        {
            IProvider objProvider = ProvderFactory.GetProvderFactory(oProviderRecordRequestModel.ProviderName, _ProviderService, _configuration);
            string Resp = "";

            if (oProviderRecordRequestModel.type.Equals(ArchiveEnum.Start.ToString()))
            {
                Resp = objProvider.StartRecording(oProviderRecordRequestModel.Id);

            }
            else if (oProviderRecordRequestModel.type.Equals(ArchiveEnum.Stop.ToString()))
            {
                Resp = objProvider.StopRecording(oProviderRecordRequestModel.Id);
            }

            return Ok(Resp);
        }

        [HttpGet("FetchRecord")]
        public ActionResult Get(string id)
        {
            const string twilioApiKey = "SK3385bf4bd2e0153e0dab2dd4c486e829";
            const string twilioApiSecret = "yh26gexfMrN9a47X1C3fugWbnTxWdOYs";

            string authToken = "14a45efd56932db62750e93a327bd26b";
            string twilioAccountSid = "AC11f98a4a87b8cbb7567a50005abcf992";

            TwilioClient.Init(twilioAccountSid, authToken);

            //var layout = new
            //{
            //    grid = new
            //    {
            //        video_sources = new string[] { "*" }
            //    }
            //};

            //var compositionHook = CompositionHookResource.Create(
            //  friendlyName: "MyFirstCompositionHook3",
            //  audioSources: new List<string> { "*" },
            //  videoLayout: layout,
            //  statusCallback: new Uri("http://my.server.org/callbacks"),
            //  format: FormatEnum.Mp4
            //);

            //string strSid = compositionHook.Sid;

            //var compositionHook1 = CompositionHookResource.Fetch(
            //  pathSid: strSid
            //);

            //var compositions = CompositionResource.Read(
            //    roomSid: id,
            //    limit: 20
            //);

            //foreach (var record in compositions)
            //{
            //    strSid = record.Sid;
            //}

            var audioSources = new List<string> {
            "*"
        };

            var videoLayout = new Dictionary<string, Object>()
        {
            {"chess_table", new Dictionary<string, Object>()
                {
                    {"x_pos", 10},
                    {"y_pos", 0},
                    {"width", 1260},
                    {"height", 720},
                    {"max_rows", 3},
                    {"max_columns", 3},
                    {"reuse", "show_newest"},
                    {"cells_excluded", new object [] {
                        1,
                        3,
                        5,
                        7
                    }},
                    {"video_sources", new object [] {
                        "*"
                    }}
                }}
        };

            var composition = CompositionResource.Create(
                audioSources: audioSources,
                videoLayout: videoLayout,
                resolution: "1280x720",
                format: CompositionResource.FormatEnum.Mp4,
                roomSid: id
            );

            Console.WriteLine(composition.Sid);

            var compositionStatus = CompositionResource.Fetch(
            pathSid: composition.Sid
        );

            Console.WriteLine(composition.Status);

            // Retrieve media location
            string compositionSid = composition.Sid;
            string uri = $"https://video.twilio.com/v1/Compositions/{compositionSid}/Media?Ttl=3600";
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(twilioAccountSid + ":" + authToken)));
            request.AllowAutoRedirect = false;
            string responseBody = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
            var mediaLocation = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseBody)["redirect_to"];

            //// For example, download the media to a local file
            //var client = new HttpClient();
            //var response = client.GetAsync(mediaLocation);
            //using (var stream = response.Content.ReadAsStreamAsync())
            //{
            //    var fileInfo = new FileInfo("myFile.mp4");
            //    using (var fileStream = fileInfo.OpenWrite())
            //    {
            //        await stream.CopyToAsync(fileStream);
            //    }
            //}


            //var recordings = RoomRecordingResource.Read(
            //    pathRoomSid: id,
            //    limit: 20
            //);

            //string strSid = "";
            //var arrSid = new List<string>();

            //foreach (var record in recordings)
            //{
            //    strSid = record.Sid;
            //    Console.WriteLine(record.Sid);

            //    string recordingSid = strSid;
            //    string uri = $"https://video.twilio.com/v1/Recordings/{recordingSid}/Media";

            //    var request = (HttpWebRequest)WebRequest.Create(uri);
            //    request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(twilioApiKey + ":" + twilioApiSecret)));
            //    request.AllowAutoRedirect = false;
            //    string responseBody = new StreamReader(request.GetResponse().GetResponseStream()).ReadToEnd();
            //    var mediaLocation = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseBody)["redirect_to"];
            //    arrSid.Add(mediaLocation);
            //}

            return Ok(mediaLocation);

            //return Ok(Resp);
        }
        
        [HttpGet("SessionDisconnect")]
        public ActionResult GetSessionDisconnect()
        {
            int ApiKey = 47095304; // YOUR API KEY
            string ApiSecret = "3ca713b0d132be02013218a35e152a733cea4c4c";

            try
            {
                var OpenTok = new OpenTok(ApiKey, ApiSecret);

                StreamList streamList = OpenTok.ListStreams(_ProviderService.strSessionId);

                Dictionary<string, string> dictStream;
                if (_ProviderService.dictUserConnection.TryGetValue(_ProviderService.strSessionId, out dictStream))
                {
                    foreach (OpenTokSDK.Stream oStream in streamList)
                    {
                        string strConnId;
                        if (dictStream.TryGetValue(oStream.Id, out strConnId))
                        {
                            OpenTok.ForceDisconnect(_ProviderService.strSessionId, strConnId);
                        }
                    }
                }
                _ProviderService.dictUserConnection = new Dictionary<string, Dictionary<string, string>>();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            
            return Ok(null);
        }
        
        [HttpPost("PostConnectionId")]
        public ActionResult PostConnectionId(TokboxSessionModel oTokboxSessionModel)
        {
            Dictionary<string, string> dictStream;
            List<string> strConnList;
            if (_ProviderService.dictUserConnection.TryGetValue(_ProviderService.strSessionId, out dictStream))
            {
                if (dictStream.ContainsKey(oTokboxSessionModel.StreamId))
                {
                    dictStream[oTokboxSessionModel.StreamId] = oTokboxSessionModel.ConnectionId;
                }
                else
                {
                    dictStream.Add(oTokboxSessionModel.StreamId, oTokboxSessionModel.ConnectionId);
                }
            }
            else
            {
                dictStream = new Dictionary<string, string>();
                dictStream.Add(oTokboxSessionModel.StreamId, oTokboxSessionModel.ConnectionId);
                _ProviderService.dictUserConnection.Add(_ProviderService.strSessionId, dictStream);
            }
            return Ok(null);
        }
    }
}
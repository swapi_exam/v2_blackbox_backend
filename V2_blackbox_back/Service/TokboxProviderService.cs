﻿using OpenTokSDK;
using V2_blackbox_back.Interface;
using V2_blackbox_back.Model;

namespace V2_blackbox_back.Service
{
    public class TokboxProviderService : IProviderService
    {   
        private Dictionary<string, Dictionary<string, string>> _dictUserConnection;
        public Dictionary<string, Dictionary<string, string>> dictUserConnection
        {
            get { return _dictUserConnection; }
            set { _dictUserConnection = value; }
        }
        
        private string _strSessionId;
        public string strSessionId 
        {
            get { return _strSessionId; }
            set { _strSessionId = value; } 
        }

        private object _oSession;
        public object oSession
        {
            get { return _oSession; }
            set { _oSession = value; } 
        }

        public TokboxProviderService()
        {
            dictUserConnection = new Dictionary<string, Dictionary<string, string>>();
        }

        public List<string> GetUserList(string id)
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            dictUserConnection.TryGetValue(id, out list);
            return list.Values.ToList();
        }

    }
}

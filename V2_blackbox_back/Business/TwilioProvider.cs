﻿using Twilio;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Chat.V1;
using Twilio.Rest.Video.V1.Room;
using Twilio.Types;
using V2_blackbox_back.Enum;
using V2_blackbox_back.Interface;
using V2_blackbox_back.Model;

namespace V2_blackbox_back.Business
{
    public class TwilioProvider : IProvider
    {
        public ProviderResponseModel GetProvider(ProviderRequestModel oProviderRequestModel)
        {
            // These values are necessary for any access token
            // To set up environmental variables, see http://twil.io/secure
            //const string twilioAccountSid = "AC4f850810ccd67a0482b8db745bd8a6d4";
            const string twilioApiKey = "SK3385bf4bd2e0153e0dab2dd4c486e829";
            const string twilioApiSecret = "yh26gexfMrN9a47X1C3fugWbnTxWdOYs";

            string authToken = "14a45efd56932db62750e93a327bd26b";
            string twilioAccountSid = "AC11f98a4a87b8cbb7567a50005abcf992";

            TwilioClient.Init(twilioAccountSid, authToken);

            var service = ServiceResource.Create(friendlyName: "friendly_name");

            string serviceSid = service.Sid;
            string identity = oProviderRequestModel.UserId;

            // Create an Chat grant for this token

            var grant = new VideoGrant();
            grant.Room = "cool-room";

            var grants = new HashSet<IGrant> { grant };

            // Create an Access Token generator
            var token = new Token(
                twilioAccountSid,
                twilioApiKey,
                twilioApiSecret,
                identity,
                grants: grants);

            //Console.WriteLine(token.ToJwt());
            //return new JsonResult(new { token = token });
            string strToken = token.ToJwt();
            // A simple Archive (without a name)
            return (new ProviderResponseModel() { ApiKey = 0, SessionId = "", Token = strToken, ProviderName = ProviderEnum.Twilio.ToString() });
        }

        public string StartRecording(string id)
        {
            string authToken = "14a45efd56932db62750e93a327bd26b";
            string twilioAccountSid = "AC11f98a4a87b8cbb7567a50005abcf992";

            TwilioClient.Init(twilioAccountSid, authToken);
            var recordingRules = RecordingRulesResource.Update(
                rules: new List<RecordingRule>(){
                new RecordingRule(Twilio.Types.RecordingRule.TypeEnum.Include, true, null, null, null)
                },
                pathRoomSid: id
            );
            return recordingRules.RoomSid;
        }

        public string StopRecording(string id)
        {
            string authToken = "14a45efd56932db62750e93a327bd26b";
            string twilioAccountSid = "AC11f98a4a87b8cbb7567a50005abcf992";

            TwilioClient.Init(twilioAccountSid, authToken);
            var recordingRules = RecordingRulesResource.Update(
                rules: new List<RecordingRule>(){
                new RecordingRule(Twilio.Types.RecordingRule.TypeEnum.Exclude, true, null, null, null)
                },
                pathRoomSid: id
            );
            return null;
        }
    }
}

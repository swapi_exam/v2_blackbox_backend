﻿using OpenTokSDK;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using V2_blackbox_back.Enum;
using V2_blackbox_back.Interface;
using V2_blackbox_back.Model;

namespace V2_blackbox_back.Business
{
    public class TokboxProvider : IProvider
    {
        private IProviderService _ProviderService;
        private IConfiguration _configuration;

        public TokboxProvider(IProviderService ProviderService, IConfiguration configuration)
        {
            _ProviderService = ProviderService;
            _configuration = configuration;
        }

        public TokboxSessionModel SessionDetails { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ProviderResponseModel GetProvider(ProviderRequestModel oProviderRequestModel)
        {
            int ApiKey = 47095304; // YOUR API KEY
            string ApiSecret = "3ca713b0d132be02013218a35e152a733cea4c4c";

            var OpenTok = new OpenTok(ApiKey, ApiSecret);
            MediaMode mediaMode = MediaMode.ROUTED;

            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12;

            WinHttpHandler httpHandler = new WinHttpHandler();

            httpHandler.SslProtocols = SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;

            Session session = null;
            if (_ProviderService.oSession == null)
            {
                session = OpenTok.CreateSession(String.Empty, mediaMode);
                _ProviderService.oSession = session;
            }
            else
            {
                session = (Session)_ProviderService.oSession;
            }
            //session = OpenTok.CreateSession(String.Empty, mediaMode);

            // session = OpenTok.CreateSession();
            string sessionId = session.Id;
            _ProviderService.strSessionId = sessionId;

            // Create a session that uses the OpenTok Media Router (which is required for archiving)
            //var session = OpenTok.CreateSession(mediaMode: MediaMode.ROUTED);
            //// Store this sessionId in the database for later use:
            //string sessionId = session.Id;

            //// Create an automatically archived session:
            //var session = OpenTok.CreateSession(mediaMode: MediaMode.ROUTED, ArchiveMode.ALWAYS);
            //// Store this sessionId in the database for later use:
            //string sessionId = session.Id;


            //// Generate a token from a sessionId (fetched from database)
            //string token = OpenTok.GenerateToken(sessionId);

            //// Generate a token by calling the method on the Session (returned from CreateSession)
            //string token = session.GenerateToken();

            // Set some options in a token
            double inOneWeek = (DateTime.UtcNow.Add(TimeSpan.FromDays(7)).Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            string token = session.GenerateToken(role: Role.MODERATOR, expireTime: inOneWeek, data: "name=" + oProviderRequestModel.UserId);

            return (new ProviderResponseModel() { ApiKey = ApiKey, SessionId = sessionId, Token = token, ProviderName = ProviderEnum.Tokbox.ToString() });
        }

        public string StartRecording(string id)
        {
            int ApiKey = 47095304; // YOUR API KEY
            string ApiSecret = "3ca713b0d132be02013218a35e152a733cea4c4c";

            var OpenTok = new OpenTok(ApiKey, ApiSecret);

            var archive = OpenTok.StartArchive(id,null, true, true, OutputMode.COMPOSED, _configuration["TokboxSettings:Resolution"]);

            return archive.ToString();
        }

        public string StopRecording(string id)
        {
            int ApiKey = 47095304; // YOUR API KEY
            string ApiSecret = "3ca713b0d132be02013218a35e152a733cea4c4c";

            var OpenTok = new OpenTok(ApiKey, ApiSecret);

            var archive = OpenTok.StopArchive(id);

            return archive.ToString();
        }
    }
}

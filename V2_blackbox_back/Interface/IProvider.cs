﻿using V2_blackbox_back.Model;

namespace V2_blackbox_back.Interface
{
    public interface IProvider
    {
        ProviderResponseModel GetProvider(ProviderRequestModel oProviderRequestModel);
        string StartRecording(string id);
        string StopRecording(string id);
    }
}

﻿using V2_blackbox_back.Model;

namespace V2_blackbox_back.Interface
{
    public interface IProviderService
    {
        Dictionary<string, Dictionary<string, string>> dictUserConnection { get; set; }
        string strSessionId { get; set; }
        List<string> GetUserList(string id);
        Object oSession { get; set; }
    }
}

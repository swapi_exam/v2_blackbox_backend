﻿using OpenTokSDK;

namespace V2_blackbox_back.Model
{
    public class TokboxSessionModel
    {
        public string StreamId { get; set; }
        public string ConnectionId { get; set; }
    }
}

﻿namespace V2_blackbox_back.Model
{
    public class ProviderRequestModel
    {
        public string UserId { get; set; }
        public int TenantId { get; set; }
        public string ProviderName { get; set; }
    }

    public class ProviderResponseModel
    {
        public int ApiKey { get; set; }
        public string SessionId { get; set; }
        public string Token { get; set; }
        public string ProviderName { get; set; }
    }

    public class ProviderRecordRequestModel
    {
        public string Id { get; set; }
        public string type { get; set; }
        public string ProviderName { get; set; }
    }
}
